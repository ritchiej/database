CREATE USER nci WITH PASSWORD 'ncidirectory';
CREATE DATABASE nci OWNER nci;
GRANT ALL PRIVILEGES ON DATABASE nci TO nci;

\connect nci

DROP TABLE IF EXISTS genetics_professional CASCADE;
DROP TABLE IF EXISTS practice_locations CASCADE;
DROP TABLE IF EXISTS specialty CASCADE ;
DROP TABLE IF EXISTS team_service CASCADE;
DROP TABLE IF EXISTS cancer_syndrome_service CASCADE;
DROP TABLE IF EXISTS cancer_type CASCADE;
DROP TABLE IF EXISTS cancer_site CASCADE;
DROP TABLE IF EXISTS membership CASCADE;

CREATE TABLE genetics_professional (
    gp_id SERIAL PRIMARY KEY,
    sname VARCHAR(255),
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    degree VARCHAR(255),
    profession_type VARCHAR(255)
);

ALTER TABLE genetics_professional OWNER TO nci;

CREATE TABLE practice_locations (
    pl_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    institution VARCHAR(255),
    address VARCHAR(255),
    city VARCHAR(255),
    state VARCHAR(255),
    zip VARCHAR(255),
    country VARCHAR(255),
    phone VARCHAR(255),
    email VARCHAR(255),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

ALTER TABLE practice_locations OWNER TO nci;

CREATE TABLE specialty (
    s_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    specialty VARCHAR(255),
    board_certified BOOLEAN,
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

ALTER TABLE specialty OWNER TO nci;

CREATE TABLE team_service (
    ts_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    sevice VARCHAR(255),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

ALTER TABLE team_service OWNER TO nci;

CREATE TABLE cancer_syndrome_service (
    css_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    cancer VARCHAR(255),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

ALTER TABLE cancer_syndrome_service OWNER TO nci;

CREATE TABLE cancer_type (
    ct_id SERIAL PRIMARY KEY,
    css_id INTEGER NOT NULL,
    cancer_type VARCHAR(255),
    cancer_sites VARCHAR(255),
    FOREIGN KEY (css_id) REFERENCES cancer_syndrome_service(css_id)
);

ALTER TABLE cancer_type OWNER TO nci;

CREATE TABLE membership (
    m_id SERIAL PRIMARY KEY,
    gp_id INTEGER NOT NULL,
    institution VARCHAR(255),
    FOREIGN KEY (gp_id) REFERENCES genetics_professional(gp_id)
);

ALTER TABLE membership OWNER TO nci;
